from PIL import Image
import sys
try:
   import colorama;
   colorama.init();
except:
	pass;

class bcolors: # ANSI color code
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def test_image(img): # image compatibility test
	ret=1;
	if img.mode not in ('RGB', 'RGBA', 'CMYK') :	# INCLUDE ONLY 3x8bits COLOR FORMAT
		print(bcolors.WARNING+"Format de pixel incompatible"+bcolors.ENDC);
		ret=0;
	if img.format == 'JPEG' :	# EXCLUDE JPEG (compressed format unsupported)
		print(bcolors.WARNING+"Format JPEG incompatible"+bcolors.ENDC);
		ret=0;
	if ret == 1 :	# print free space if image loaded
		print(bcolors.OKBLUE+"Stockage disponible : "+str(img.width*img.height*3*8)+" bits"+bcolors.ENDC);
	return ret;

def load_image(img): # load image and test it
	try:
		copie=Image.copy(img);
		if test_image() :
			return copie;
		else:
			return 0;
	except:
		return 0;

def encode
	#TODO
# data = list(image.getdata()) -> pixels values

def menu(): # UI
	bExitFlag=0;
	copie=0;
	message="";
	clear();
	print(bcolors.HEADER+"\n#################\n# Steganographie #\n#################"+bcolors.ENDC);
	while not bExitFlag :
		print("\n~ Menu ~\n1 : Charger une image\n2 : Selectionner un message\n3 : Encoder\n"+bcolors.BOLD+"4 : Quitter\n"+bcolors.ENDC);
		sPrompt=raw_input("#: ");
		clear();
		if sPrompt == "1" : # IMAGE
			print("\n~ Charger une image ~\n");
			sPrompt=raw_input("Donnez le nom de l'image : ");
			copie=load_image(sPrompt);
			if copie == 0 :
				print(bcolors.FAIL+"!! Aucune image chargee car une erreur est survenue !!"+bcolors.ENDC);
			else :
				print(bcolors.OKGREEN+"Image chargee avec succes !"+bcolors.ENDC);
		elif sPrompt == "2" :	# MESSAGE
			print("\n~ Selectionner un message ~\n");
			print("1 : Charger un fichier\n2 : Entrer un text brut\n"+bcolors.BOLD+"3 : Anuler"+bcolors.ENDC);
			sPrompt=raw_input("#: ");
			if sPrompt == "1" :
				if doc :
					doc.close;
				message="";
				sPrompt=raw_input("Donnez le nom du fichier : ");
				try:
					doc=open(sPrompt ,r ,-1);
				except:
					print(bcolors.FAIL+"!! Aucun fichier portant ce nom !!"+bcolors.ENDC);
			elif sPrompt == "2" :
				if doc :
					doc.close;
				message=raw_input("Votre text : ");
		elif sPrompt == "3" :	# ENCODE
			print("\n~ Encoder ~\n");
			print("Selectionnez un methode d'encodage :\n1 : Brute");
			print(bcolors.WARNING+"! Les methodes suivantes peuvent corrompre le message si les types ne correspondent pas !"+bcolors.ENDC);
			print("2 : Numerique\n3 : Text simplifie\n4 : Image degradee\n"+bcolors.BOLD+"5 : Anuler"+bcolors.ENDC);
			sPrompt=raw_input("#: ");
			if sPrompt is in ('1','2','3','4') :
				if message != "" and copie != 0 :
					pass	#TODO: add encode function
				elif doc and copie != 0 :
					pass	#TODO: add encode function
				else
					print(bcolors.WARNING+"! Vous devez charger une image et un message avant d'encoder !"+bcolors.ENDC);
		elif sPrompt == "4" :	# QUIT
			bExitFlag=1;
		else :
			print(bcolors.WARNING+"! \""+sPrompt+"\" n'est pas une option valide !\n"+bcolors.ENDC);
	if doc :
		doc.close;
	clear();

if __name__ == "__main__" :
	menu();