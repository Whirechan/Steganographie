Les images sont là pour expliquer le principe général des différentes méthodes,
et non pour montrer chaque application du principe.
Aucune image prévue pour les formats compressés avec pertes car non représentable graphiquement.

Voir le fichier TODO et les tag #TODO dans les fichiers pour connaitre les prochains ajouts.